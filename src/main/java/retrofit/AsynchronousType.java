package retrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AsynchronousType {
    public static void main(String[] args){

        /*
        *
        * Defining API Key inside source code, please use the environment variable instead.
        * */
        String omdbApiKey = "b682db52";
        //String omdbApiKey = System.getenv("OMDB_KEY");

        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://www.omdbapi.com")
                .addConverterFactory(GsonConverterFactory.create()).build();

        OMDBEndpoints omdbService = retrofit.create(OMDBEndpoints.class);
        Call<OMDBSearchResult> apiCall = omdbService
                .getMovieListFilteredByTitle("Age of Ultron", omdbApiKey);

        apiCall.enqueue(new Callback<OMDBSearchResult>() {

            @Override
            public void onResponse(Call<OMDBSearchResult> call, Response<OMDBSearchResult> response) {
                    if (response.isSuccessful()){
                        OMDBSearchResult resultInstance = response.body();
                        System.out.println(resultInstance);
                    } else{
                        System.out.println(response.message());
                    }
            }

            @Override
            public void onFailure(Call<OMDBSearchResult> call, Throwable t) {
                System.out.println(t.getMessage());
            }

        });
        System.out.println("Doing something 1...2...3...");
        System.out.println("Doing something 1...2...3...");
        System.out.println("Doing something 1...2...3...");
    }
}
