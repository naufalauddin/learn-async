package manual;

import java.util.concurrent.Callable;

class FactorialTask implements Callable<Integer> {

    int number;

    public FactorialTask(int number) {
        this.number = number;
    }

    @Override
    public Integer call() throws Exception {
        int fact = 1;
        for (int count = number; count > 1 ; count--) {
            fact = fact * count;
            Thread.sleep(1000);
        }

        return fact;
    }
}
